% Let's Talk Data
% Savanni D'Gerinel
% 2016-02-02

Introductions
=============
*   Savanni D'Gerinel
*   zie/hir

Homework
========


Possible Directions
===================

*   Data Analysis
*   Web Development
*   Computer Graphics
*   Computer Music
*   Game Development

Enumeration
===========
~~~~ {.haskell}
data Activity = Cycling | Running
~~~~

Structure of a Data Type
========================
~~~~ {.haskell}
data Activity = Cycling | Running
~~~~

    data Activity = ...
         <type constructor>

Structure of a Data Type
========================
~~~~ {.haskell}
data Activity = Cycling | Running
~~~~

    data Activity = ...
         <type constructor>

    ... = Cycling | Running
          <data constructors>

Enumeration
===========
~~~~ {.haskell}
data Activity = Cycling | Running
~~~~

    data Activity = ...
         <type constructor>

    ... = Cycling | Running
          <data constructors>

~~~~
*Demo> :t Cycling
Cycling :: Activity

*Demo> :t Running
Running :: Activity

*Demo> Running
Running

*Demo> let act = Cycling
*Demo> :t act
act :: Activity
*Demo> act
Cycling
~~~~

Python
======
~~~~ {.python}
class Activity (object):
    def __init__ (self, val):
        if val in ["Cycling", "Running"]:
            self._val = val
        else:
            raise ValueError(val)

    @staticmethod
    def Cycling (): return Activity("Cycling")

    @staticmethod
    def Running (): return Activity("Running")

    def __repr__ (self): return self._val
~~~~

Python
======
~~~~ {.python}
class Activity (object):
    def __init__ (self, val):
        if val in ["Cycling", "Running"]:
            self._val = val
        else:
            raise ValueError(val)

    @staticmethod
    def Cycling (): return Activity("Cycling")

    @staticmethod
    def Running (): return Activity("Running")

    def __repr__ (self): return self._val
~~~~

~~~~
>>> import demo
>>> demo.Activity.Cycling()
Cycling
>>> a = demo.Activity.Running()
>>> a
Running
~~~~

Record
======
~~~~ {.haskell}
data Workout = Workout UTCTime Activity Float Int Text
~~~~
         <type constructor>                         <fields>
    data Workout =            Workout               UTCTime Activity Float Int Text
                              <value constructor>

Record
======
~~~~ {.haskell}
data Workout = Workout UTCTime Activity Float Int Text
~~~~
         <type constructor>                         <fields>
    data Workout =            Workout               UTCTime Activity Float Int Text
                              <value constructor>

~~~~
*Demo> :t Workout
Workout :: UTCTime -> Activity -> Float -> Int -> String -> Workout
~~~~

Record
======
~~~~ {.haskell}
data Workout = Workout UTCTime Activity Float Int Text
~~~~
         <type constructor>                         <fields>
    data Workout =            Workout               UTCTime Activity Float Int Text
                              <value constructor>

~~~~
*Demo> :t Workout
Workout :: UTCTime -> Activity -> Float -> Int -> String -> Workout
*Demo> now <- getCurrentTime
*Demo> Workout now Cycling 12.2 25 "Class Demo"
Workout 2016-02-01 04:56:47.519675 UTC Cycling 12.2 25 "Class Demo"
~~~~

Record
======
~~~~ {.haskell}
data Workout2 = Workout2 { w2_time     :: UTCTime
                         , w2_activity :: Activity
                         , w2_distance :: Float
                         , w2_duration :: Int
                         , w2_comments :: String
                         }
                         deriving (Eq, Show)
~~~~

~~~~
*Demo> Workout2 now Cycling 12.2 25 "Class Demo"
Workout2 {w2_time = 2016-02-01 05:04:35.829597 UTC,
          w2_activity = Cycling,
          w2_distance = 12.2,
          w2_duration = 25.0,
          w2_comments = "Class Demo"}
~~~~

Put them together
=================
~~~~ {.haskell}
data Workout3 =
      TimeDistance { w3_time     :: UTCTime
                   , w3_actvity  :: Activity
                   , w3_distance :: Float
                   , w3_duration :: Int
                   , w3_comments :: String
                   }
    | Sports { w3_time     :: UTCTime
             , w3_sport    :: Sport
             , w3_duration :: Int
             }
    deriving (Eq, Show)
~~~~

Put them together
=================
~~~~ {.haskell}
data Workout3 =
      TimeDistance { w3_time     :: UTCTime
                   , w3_actvity  :: Activity
                   , w3_distance :: Float
                   , w3_duration :: Int
                   , w3_comments :: String
                   }
    | Sports { w3_time     :: UTCTime
             , w3_sport    :: Sport
             , w3_duration :: Int
             }
    deriving (Eq, Show)
~~~~

~~~~
*Demo> :t TimeDistance
TimeDistance
  :: UTCTime -> Activity -> Float -> Int -> String -> Workout3
*Demo> :t Sports
Sports :: UTCTime -> Sport -> Int -> Workout3
~~~~

Fields
======
~~~~
*Demo> :t w3_time
w3_time :: Workout3 -> UTCTime
*Demo> :t w3_comments
w3_comments :: Workout3 -> String
~~~~

Incompleteness
==============
~~~~
*Demo> now <- getCurrentTime
*Demo> let bikeride = TimeDistance now Cycling 6.45 1328 "Ride to work"
*Demo> let playtime = Sports now Soccer 3600
~~~~

Incompleteness
==============
~~~~
*Demo> now <- getCurrentTime
*Demo> let bikeride = TimeDistance now Cycling 6.45 1328 "Ride to work"
*Demo> let playtime = Sports now Soccer 3600
*Demo> w3_time bikeride
2016-02-02 16:32:08.717093 UTC
*Demo> w3_time playtime
2016-02-02 16:32:08.717093 UTC
~~~~

Incompleteness
==============
~~~~
*Demo> now <- getCurrentTime
*Demo> let bikeride = TimeDistance now Cycling 6.45 1328 "Ride to work"
*Demo> let playtime = Sports now Soccer 3600
*Demo> w3_time bikeride
2016-02-02 16:32:08.717093 UTC
*Demo> w3_time playtime
2016-02-02 16:32:08.717093 UTC
*Demo> w3_distance bikeride
6.45
~~~~

Incompleteness
==============
~~~~
*Demo> now <- getCurrentTime
*Demo> let bikeride = TimeDistance now Cycling 6.45 1328 "Ride to work"
*Demo> let playtime = Sports now Soccer 3600
*Demo> w3_time bikeride
2016-02-02 16:32:08.717093 UTC
*Demo> w3_time playtime
2016-02-02 16:32:08.717093 UTC
*Demo> w3_distance bikeride
6.45
*Demo> w3_distance playtime
*** Exception: No match in record selector w3_distance
*Demo>
~~~~

Kindedness
==========
~~~~ {.haskell}
data Workout3 =
      TimeDistance { time :: UTCTime
                   , actvity :: Activity
                   , distance :: Float
                   , duration :: Int
                   , comments :: String
                   }
    | Sports { time :: UTCTime
             , sport :: Sport
             , duration :: Int
             }
    deriving (Eq, Show)
~~~~

~~~~
*Demo> :k Workout3
Workout3 :: *
~~~~

Higher Order Types
==================
~~~~ {.haskell}
data Maybe a = Nothing | Just a
~~~~

Higher Order Types
==================
~~~~ {.haskell}
data Maybe a = Nothing | Just a
~~~~

    data Maybe a = ...
         <type constructor, parameterized over a>

    ... | Just a
          <this is the same a>

    Just 15 :: Maybe Int
    Just "Savanni" :: Maybe String

Higher Order Types
==================
~~~~ {.haskell}
data Maybe a = Nothing | Just a
~~~~

    data Maybe a = ...
         <type constructor, parameterized over a>

    ... | Just a
          <this is the same a>

    Just 15 :: Maybe Int
    Just "Savanni" :: Maybe String

~~~~
*Demo> :k Maybe
Maybe :: * -> *
~~~~

Higher Order Types
==================
~~~~ {.haskell}
data Maybe a = Nothing | Just a
~~~~

    data Maybe a = ...
         <type constructor, parameterized over a>

    ... | Just a
          <this is the same a>

    Just 15 :: Maybe Int
    Just "Savanni" :: Maybe String

~~~~
*Demo> :k Maybe
Maybe :: * -> *
*Demo> :t Just "Savanni"
Just "Savanni" :: Maybe [Char]
*Demo> :t Nothing
Nothing :: Maybe a
*Demo> :t Nothing :: Maybe String
Nothing :: Maybe String :: Maybe String
~~~~

Higher Order Types
==================
~~~~ {.haskell}
data Either l r = Left l | Right r
~~~~

    data Either l r
         <type constructor, parameterized over l and r>

    ... Left l
        <this is the Left constructor, using type l>

    ... Right r
        <this is the Right constructor, using time r>

~~~~
*Demo> :k Either
Either :: * -> * -> *
*Demo> :t Left "abcd"
Left "abcd" :: Either [Char] r
*Demo> :t Right (15 :: Int)
Right (15 :: Int) :: Either a Int
~~~~


Data Destructuring: Maybe
=========================
~~~~
*Demo> :t maybe
maybe :: b -> (a -> b) -> Maybe a -> b
*Demo> maybe (15 :: Int) (+1) Nothing
15
*Demo> maybe (15 :: Int) (+1) (Just 25)
26
~~~~

Data Destructuring: Maybe
=========================
~~~~
*Demo> :t maybe
maybe :: b -> (a -> b) -> Maybe a -> b
*Demo> maybe (15 :: Int) (+1) Nothing
15
*Demo> maybe (15 :: Int) (+1) (Just 25)
26
~~~~

~~~~ {.haskell}
maybe ::  b       ->  (a -> b) -> Maybe a     ->  b
maybe     default     func        Nothing     =   default
maybe     default     func        (Just val)  =   func val
~~~~

Data Destructuring: Maybe
=========================
~~~~
*Demo> :t maybe
maybe :: b -> (a -> b) -> Maybe a -> b
*Demo> maybe (15 :: Int) (+1) Nothing
15
*Demo> maybe (15 :: Int) (+1) (Just 25)
26
~~~~

~~~~ {.haskell}
maybe ::  b       ->  (a -> b) -> Maybe a     ->  b
maybe     default     _           Nothing     =   default
maybe     _           func        (Just val)  =   func val
~~~~

Data Destructuring: Either
==========================
~~~~
*Demo> :t either
either :: (a -> c) -> (b -> c) -> Either a b -> c
*Demo> either (\_ -> (-1)) (\x -> x + 15) (Left "oops")
-1
*Demo> either (\_ -> (-1)) (\x -> x + 15) (Right 99)
114
~~~~

Data Destructuring: Either
==========================
~~~~
*Demo> :t either
either :: (a -> c) -> (b -> c) -> Either a b -> c
*Demo> either (\_ -> (-1)) (\x -> x + 15) (Left "oops")
-1
*Demo> either (\_ -> (-1)) (\x -> x + 15) (Right 99)
114
~~~~

~~~~ {.haskell}
either :: (a -> c) -> (b -> c) -> Either a c    ->  c
either    ifLeft      ifRight     (Left lval)   =   ifLeft lval
either    ifLeft      ifRight     (Right rval)  =   ifRight rval
~~~~

Data Destructuring: Either
==========================
~~~~
*Demo> :t either
either :: (a -> c) -> (b -> c) -> Either a b -> c
*Demo> either (\_ -> (-1)) (\x -> x + 15) (Left "oops")
-1
*Demo> either (\_ -> (-1)) (\x -> x + 15) (Right 99)
114
~~~~

~~~~ {.haskell}
either :: (a -> c) -> (b -> c) -> Either a c    ->  c
either    ifLeft      _           (Left lval)   =   ifLeft lval
either    _           ifRight     (Right rval)  =   ifRight rval
~~~~



Resources
=========
*   [Code of Conduct](https://gitlab.com/savannidgerinel/feminists-hack-haskell/blob/sol/CodeOfConduct.md)
*   [https://gitlab.com/savannidgerinel/feminists-hack-haskell](https://gitlab.com/savannidgerinel/feminists-hack-haskell)
