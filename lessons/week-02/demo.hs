module Demo where

import Data.Time.Clock (UTCTime, getCurrentTime)
import Data.Time.LocalTime ()

data Activity = Cycling | Running
              deriving (Eq, Show, Read)

data Sport = Soccer | Basketball | UltimateFrisbee
           deriving (Eq, Show, Read)

data Workout = Workout UTCTime Activity Float Int String
    deriving (Eq, Show)

data Workout2 = Workout2 {
      w2_time :: UTCTime
    , w2_activity :: Activity
    , w2_distance :: Float
    , w2_duration :: Int
    , w2_comments :: String
    }
    deriving (Eq, Show)

data Workout3 =
      TimeDistance { w3_time :: UTCTime
                   , w3_actvity :: Activity
                   , w3_distance :: Float
                   , w3_duration :: Int
                   , w3_comments :: String
                   }
    | Sports { w3_time :: UTCTime
             , w3_sport :: Sport
             , w3_duration :: Int
             }
    deriving (Eq, Show)
