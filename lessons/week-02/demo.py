
class Activity (object):
    def __init__ (self, val):
        if val in ["Cycling", "Running"]:
            self._val = val
        else:
            raise ValueError(val)

    @staticmethod
    def Cycling (): return Activity("Cycling")

    @staticmethod
    def Running (): return Activity("Running")

    def __repr__ (self): return self._val
