% Introduction
% Savanni D'Gerinel
% 2016-01-18

Welcome
=======
*   [www.savannidgerinel.com](http:/www.savannidgerinel.com/)
*   Software Engineer at [EnergyCurb](http://www.energycurb.com/)
*   @savannidgerinel
*   savanni.dgerinel@gmail.com
*   Code witch, photographer, cyclist, agender, feminist, radical queer, general geek

Welcome
=======
*   Your name
*   Your pronouns
*   Your interest in Haskell

Code of Conduct
===============
[Code of Conduct](https://gitlab.com/savannidgerinel/feminists-hack-haskell/blob/sol/CodeOfConduct.md)

Repository
==========
[https://gitlab.com/savannidgerinel/feminists-hack-haskell](https://gitlab.com/savannidgerinel/feminists-hack-haskell)

Stack
=====
Time to get Stack set up!

    stack ghci

Functions
=========
~~~~ {.haskell}
pi = 3.1415926
fFromC c = c * 1.8 + 32
~~~~

Functions
=========
~~~~ {.haskell}
Prelude> let pi = 3.1415926
Prelude> pi
3.1415926
Prelude> let fFromC c = c * 1.8 + 32
Prelude> fFromC 0
32.0
Prelude> fFromC 37
98.60000000000001
Prelude> fFromC 100
212.0
~~~~

Primitives
==========
*   Int, Integer
*   Float, Double
*   Char, String
*   Bool

Primitives
==========
~~~~ {.haskell}
Prelude> :t 15
15 :: Int
Prelude> :t 15.5
15.5 :: Float
Prelude> :t 'a'
'a' :: Char
Prelude> :t "abcdefg"
"abcdefg" :: String
~~~~

Groupings
=========
*   Tuples -- `(Int, Float, String)`
*   Lists -- `[Int]`
*   List of tuples -- `[(Int, String)]`

Groupings
=========
~~~~ {.haskell}
Prelude> :t (15, 22, "whatever")
(15, 22, "whatever") :: (Int, Int, String)
~~~~

Groupings
=========
~~~~ {.haskell}
Prelude> :t (15, 22, "whatever")
(15, 22, "whatever") :: (Int, Int, String)
Prelude> :t [15, 22, 25, 70]
[15, 22, 25, 70] :: [Int]
~~~~

Groupings
=========
~~~~ {.haskell}
Prelude> :t (15, 22, "whatever")
(15, 22, "whatever") :: (Int, Int, String)
Prelude> :t [15, 22, 25, 70]
[15, 22, 25, 70] :: [Int]
Prelude> :t [15, 22, 25, 70, "whatever"]

<interactive>:1:2:
    No instance for (Num [Char]) arising from the literal ‘15’
    In the expression: 15
    In the expression: [15, 22, 25, 70, ....]
~~~~

Groupings
=========
~~~~ {.haskell}
Prelude> :t (15, 22, "whatever")
(15, 22, "whatever") :: (Int, Int, String)
Prelude> :t [15, 22, 25, 70]
[15, 22, 25, 70] :: [Int]
Prelude> :t [15, 22, 25, 70, "whatever"]

<interactive>:1:2:
    No instance for (Num [Char]) arising from the literal ‘15’
    In the expression: 15
    In the expression: [15, 22, 25, 70, ....]

Prelude> :t [("savanni", 37), ("brandi", 18)]
[("savanni", 37), ("brandi", 18)] :: [(String, Int)]
~~~~

Groupings
=========
~~~~ {.haskell}
Prelude> :t (15, 22, "whatever")
(15, 22, "whatever") :: (Int, Int, String)
Prelude> :t [15, 22, 25, 70]
[15, 22, 25, 70] :: [Int]
Prelude> :t [15, 22, 25, 70, "whatever"]

<interactive>:1:2:
    No instance for (Num [Char]) arising from the literal ‘15’
    In the expression: 15
    In the expression: [15, 22, 25, 70, ....]

Prelude> :t [("savanni", 37), ("brandi", 18)]
[("savanni", 37), ("brandi", 18)] :: [(String, Int)]
Prelude> :t "abcdefg"
"abcdefg" :: [Char]
~~~~

Type signatures
===============
~~~~ {.haskell}
Prelude> :t 15
15 :: Int
Prelude> :t fFromC
fFromC :: Float -> Float
~~~~

Basic Functions
===============
~~~~ {.haskell}
(+), (-), (*), div, mod :: Int -> Int -> Int
(/) :: Float -> Float -> Float

map ::    (a -> b)      -> [a]  -> [b]
filter :: (a -> Bool)   -> [a]  -> [a]
foldl ::  (b -> a -> b) -> b    -> [a] -> [b]
putStrLn :: String -> IO ()

lookup :: a -> [(a, b)] -> Maybe b
~~~~

Maybes!
=======
Hey, guess what! Haskell has no NULL or None values!

Maybes!
=======
Hey, guess what! Haskell has no NULL or None values!

~~~~ {.haskell}
Prelude> Nothing
Nothing
Prelude> Just 15
Just 15
Prelude> :t Nothing
Nothing :: Maybe a
Prelude> :t Just 15
Just 15 :: Maybe Int
~~~~

Maybes!
=======
Hey, guess what! Haskell has no NULL or None values!

~~~~ {.haskell}
Prelude> Nothing
Nothing
Prelude> Just 15
Just 15
Prelude> :t Nothing
Nothing :: Maybe a
Prelude> :t Just 15
Just 15 :: Maybe Int

Prelude> :t lookup
lookup :: Eq a => a -> [(a, b)] -> Maybe b
Prelude> let genderList = [("savanni", "agender"), ("kylie", "woman")]
~~~~

Maybes!
=======
Hey, guess what! Haskell has no NULL or None values!

~~~~ {.haskell}
Prelude> Nothing
Nothing
Prelude> Just 15
Just 15
Prelude> :t Nothing
Nothing :: Maybe a
Prelude> :t Just 15
Just 15 :: Maybe Int

Prelude> :t lookup
lookup :: Eq a => a -> [(a, b)] -> Maybe b
Prelude> let genderList = [("savanni", "agender"), ("kylie", "woman")]
Prelude> lookup "savanni" genderList
Just "agender"
~~~~

Maybes!
=======
Hey, guess what! Haskell has no NULL or None values!

~~~~ {.haskell}
Prelude> Nothing
Nothing
Prelude> Just 15
Just 15
Prelude> :t Nothing
Nothing :: Maybe a
Prelude> :t Just 15
Just 15 :: Maybe Int

Prelude> :t lookup
lookup :: Eq a => a -> [(a, b)] -> Maybe b
Prelude> let genderList = [("savanni", "agender"), ("kylie", "woman")]
Prelude> lookup "savanni" genderList
Just "agender"
Prelude> lookup "kathryn" genderList
Nothing
~~~~

Algebraic Data Types
====================
~~~~ {.haskell}
data Maybe a = Nothing | Just a
~~~~

Algebraic Data Types
====================
~~~~ {.haskell}
data Maybe a = Nothing | Just a
~~~~

Core feature of the language... but not tonight

IO ()
=====
~~~~ {.haskell}
putStrLn :: String -> IO ()
~~~~

IO ()
=====
~~~~ {.haskell}
putStrLn :: String -> IO ()
~~~~

Critical, but super complicated. Not tonight.

Function composition and partial application
============================================
~~~~ {.haskell}
Prelude> :t lookup
lookup :: Eq a => a -> [(a, b)] -> Maybe b
~~~~

Function composition and partial application
============================================
~~~~ {.haskell}
Prelude> :t lookup
lookup :: Eq a => a -> [(a, b)] -> Maybe b
Prelude> :t lookup "savanni"
lookup "savanni" :: [(String, b)] -> Maybe b
~~~~

Function composition and partial application
============================================
~~~~ {.haskell}
Prelude> :t lookup
lookup :: Eq a => a -> [(a, b)] -> Maybe b
Prelude> :t lookup "savanni"
lookup "savanni" :: [(String, b)] -> Maybe b
Prelude> let ls = lookup "savanni"
Prelude> :t ls
ls :: [([Char], b)] -> Maybe b
~~~~

map, filter, foldl
==================
~~~~ {.haskell}
map fFromC temperatures :: [Float]
*Main> :t map fFromC
map fFromC :: [Float] -> [Float]
*Main>
*Main>
*Main>
*Main> :t map
map :: (a -> b) -> [a] -> [b]
*Main> :t map fFromC
map fFromC :: [Float] -> [Float]
*Main> :t map fFromC temperatures
map fFromC temperatures :: [Float]
*Main> map fFromC temperatures
[32.0,89.6,98.6,212.0]
*Main> :t map fFromC
map fFromC :: [Float] -> [Float]
*Main> let tempChanger = map fFromC
*Main> :t tempChanger
tempChanger :: [Float] -> [Float]
*Main> tempChanger temperatures
[32.0,89.6,98.6,212.0]
*Main> :t temperatures
temperatures :: [Float]
*Main> temperatures
[0.0,32.0,37.0,100.0]
*Main> let notFreezing t = t > 32
*Main> :t notFreezing
notFreezing :: (Num a, Ord a) => a -> Bool
*Main> let notFreezing t = t > 32
*Main> :t notFreezing
notFreezing :: (Num a, Ord a) => a -> Bool
*Main> :t filter notFreezing
filter notFreezing :: (Num a, Ord a) => [a] -> [a]
*Main> :t filter
filter :: (a -> Bool) -> [a] -> [a]
*Main> :t filter notFreezing
filter notFreezing :: (Num a, Ord a) => [a] -> [a]
*Main> notFreezing 10
False
*Main> notFreezing 34
True
*Main> let ftemps = map fFromC temperatures
*Main> ftemps
[32.0,89.6,98.6,212.0]
*Main> filter notFreezing ftemps
[89.6,98.6,212.0]
*Main> let notFreezing t = t > 32
*Main> let t = 15
*Main> let t = 20
*Main> :t (+)
(+) :: Num a => a -> a -> a
*Main> :t foldl
foldl :: Foldable t => (b -> a -> b) -> b -> t a -> b
*Main> :t foldl (+)
foldl (+) :: (Num b, Foldable t) => b -> t b -> b
*Main> foldl (+) 0 []
0
*Main> foldl (+) 0 [15, 30]
45
*Main> foldl (+) 70 [15, 30]
~~~~
