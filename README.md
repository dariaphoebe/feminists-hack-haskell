# Feminists Hack Haskell

## About

A study group for women and nonbinary people interested in programming with Haskell, Purescript, and closely related languages. We meet every two weeks at my office at Curb, and all meetings are broadcast through Citrix GoToMeeting. This mailing list is for talking about our homework, discussing code reviews, asking questions, and providing support.

## Code Of Conduct

Please familiarize yourself with the [Code of Conduct](CodeOfConduct.md) before attending any classes or participating in any online activities. You are bound by the code of conduct immediately upon engaging with this group in any way and you will be held responsible for any violations of the code. Ignorance is not considered an acceptable excuse for violations of the code.

## Structure

Classes will meet every two weeks. Generally we will cover the homework for the last two week period, and then have a combination of lecture and live coding session.

Homework and reading will come from the [CIS 194, Spring Semester](http://www.cis.upenn.edu/~cis194/spring13/) course. All material is provided in this repository. Follow the instructions in the [homework README](cis194/README.md) for how to share your work.

All classes will be broadcast on Citrix GoToMeeting. The meeting ID number will be posted to both the Google mailing list and to the Feminist Hack ATX announcement page fifteen minutes before class starts.

Lecture notes and presentation will generally be posted to the group repository before class begins, but may have to wait for a day or so after. Sorry, I am human and have a lot on my plate.

## Online Resources

*   [Homework and Lecture Repository](git@gitlab.com:savannidgerinel/feminists-hack-haskell.git)
*   [Google Discussion Group](https://groups.google.com/forum/#!forum/feminists-hack-haskell)
